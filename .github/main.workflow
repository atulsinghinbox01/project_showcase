workflow "Push" {
  on = "push"
  resolves = ["Draft Release"]
}

action "Draft Release" {
  uses = "toolmantim/release-drafter@master"
  secrets = ["GITHUB_TOKEN"]
}
