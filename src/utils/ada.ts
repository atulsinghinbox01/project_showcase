import { trackEvent } from './tracking';

/**
 * Method that can be called after a component is mounted to set the chatbot parent
 * element to something other than the default side drawer.
 *
 * Refer to: https://adasupport.github.io/documentation/#parentelement.
 *
 * @param parentElement element id or HTML element of desired parent element
 */
export const setChatbotParentElement = (parentElement: string | HTMLElement) => {
  window.adaSettings = {
    parentElement,
  };
};

/**
 * Method that can be called after a component is mounted to start
 * tracking when user toggles chatbot open or closed.
 *
 * Refer to: https://adasupport.github.io/documentation/#togglecallback
 */
export const setToggleCallbackToTrackToggle = () => {
  window.adaSettings = {
    toggleCallback: (isDrawerOpen: boolean) => {
      if (isDrawerOpen) {
        trackEvent('Ada Chatbot', 'Chatbot Opened');
      } else {
        trackEvent('Ada Chatbot', 'Chatbot Minimized');
      }
    },
  };
};
