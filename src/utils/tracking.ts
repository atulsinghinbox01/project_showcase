export enum ButtonType {
  DOWNLOAD_BUTTON = 'download_button',
  SIGN_UP_BUTTON = 'sign_up_button',
}

export const trackEvent = (category: string, action: string, label?: string) => {
  // eslint-disable-next-line no-undef
  analytics.track(action, {
    category,
    label,
  });
};

export const trackConversion = (type: ButtonType) => {
  window.tatari.track('click', {
    type,
  });
  window.gtag('event', 'conversion', { send_to: 'AW-819424404/ISqQCN-Ew4MDEJTZ3YYD' });
};
