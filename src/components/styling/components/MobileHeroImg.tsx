import styled from 'styled-components';
import { device } from '../sizing';

const MobileHeroImg = styled.img`
  width: 90%;
  @media ${device.tablet} {
    width: 50%;
  }
`;

export default MobileHeroImg;
