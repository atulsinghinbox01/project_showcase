import styled from 'styled-components';
import { Color, Font } from '../theme';

// <a> for nav links for now
interface NavLinkProps {
  readonly primary?: boolean;
}

const NavBarLink = styled.a`
  font-family: ${Font.BOLD};
  font-size: 1rem;
  color: ${(props: NavLinkProps) => (props.primary ? Color.PRIMARY : Color.DARK)};
  :hover {
    color: ${(props: NavLinkProps) => (props.primary ? Color.PRIMARY : Color.DARK)};
  }
`;

export default NavBarLink;
