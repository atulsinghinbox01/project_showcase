import styled from 'styled-components';
import { device } from './sizing';
import { Color, Font } from './theme';


interface TypographyProps {
  white?: boolean;
  dark?: boolean;
  bold?: boolean;
  large?: boolean;
}

// Typography
// Header 1
export const H1 = styled.h1<TypographyProps>`
  font-family: ${Font.BOLD};
  font-size: 1.875rem;
  line-height: 110%;
  letter-spacing: -0.02em;
  color: ${Color.DARK};

  @media ${device.laptop} {
    font-size: 3.5rem;
  }
`;

// Header 1 Small (use larger breakpoint)
export const H1Small = styled.h1<TypographyProps>`
  font-family: ${Font.BOLD};
  font-size: 1.875rem;
  line-height: 110%;
  letter-spacing: -0.02em;
  color: ${Color.DARK};

  @media ${device.laptopL} {
    font-size: 3.5rem;
  }
`;

// Header 2
export const H2 = styled.h2<TypographyProps>`
  font-family: ${Font.BOLD};
  font-size: 1.75rem;
  color: ${(props) => (props.white ? Color.WHITE : Color.DARK)};
  line-height: 110%;
  letter-spacing: -0.02em;

  @media ${device.laptopL} {
    font-size: 2.5rem;
    line-height: 3.1875rem;
  }
`;

// Header 3
export const H3 = styled.h3<TypographyProps>`
  font-family: ${Font.BOLD};
  font-size: 1.375rem;
  color: ${Color.DARK};
  line-height: 1.5rem;

  @media ${device.laptopL} {
    font-size: 1.5rem;
    line-height: 1.75rem;
  }
`;

// Header 4
export const H4 = styled.h4<TypographyProps>`
  font-family: ${Font.BOLD};
  font-size: 1.25rem;
  color: ${Color.DARK};
  line-height: 110%;

  @media ${device.laptopL} {
    font-size: 1.375rem;
    line-height: 1.875rem;
  }
`;

// Header 5 - used for footer section titles
export const H5 = styled.h5<TypographyProps>`
  font-family: ${Font.BOLD};
  font-size: 1.375rem;
  color: ${Color.WHITE};
  line-height: 110%;
  letter-spacing: -0.02em;

  @media ${device.laptopL} {
    font-size: 1.125rem;
    line-height: 180%;
  }
`;

export const P = styled.p<TypographyProps>`
  font-family: ${Font.REGULAR};
  font-size: ${(props) => (props.large ? `.875rem` : `.9375rem`)};
  color: ${Color.SEMIDARK};
  line-height: 180%;

  @media ${device.laptopL} {
    font-size: ${(props) => (props.large ? `1.125rem` : `1rem`)};
  }
`;

export const P1 = styled.p<TypographyProps>`
  font-family: ${(props) => (props.bold ? Font.BOLD : Font.REGULAR)};
  color: ${(props) => {
    if (props.white) {
      return Color.WHITE;
    } else if (props.dark) {
      return Color.DARK;
    } else {
      return Color.SEMIDARK;
    }
  }};
  font-size: 1rem;
  line-height: 1.625rem;

  @media ${device.tablet} {
    line-height: 1.625rem;
  }

  @media ${device.laptopL} {
    font-size: 1.125rem;
    line-height: 2rem;
  }
`;

export const P2 = styled.p<TypographyProps>`
  font-family: ${Font.REGULAR};
  color: ${Color.SEMIDARK};
  font-size: 0.875rem;
  line-height: 1.625rem;

  @media ${device.tablet} {
    font-size: 1rem;
    line-height: 1.125rem;
  }
`;

export const P3 = styled.p<TypographyProps>`
  font-family: ${Font.REGULAR};
  color: ${(props) => (props.white ? Color.WHITE : Color.SEMIDARK)};
  font-size: 0.75rem;
  line-height: 1.125rem;
  @media ${device.tablet} {
    line-height: 1.25rem;
  }
`;

export const Link = styled.a<TypographyProps>`
  font-family: ${Font.BOLD};
  color: ${Color.PRIMARY};
  font-size: 1rem;
  line-height: 180%;

  @media ${device.laptopL} {
    line-height: 1.375rem;
  }
  :hover {
    text-decoration: underline;
    color: ${Color.PRIMARY};
  }
`;

export const FooterLink = styled.a<TypographyProps>`
  font-family: ${Font.REGULAR};
  color: ${Color.WHITE};
  font-size: 0.9375rem;
  line-height: 180%;

  @media ${device.laptopL} {
    font-size: 1rem;
  }

  :hover {
    color: ${Color.WHITE};
    text-decoration: none;
    font-weight: bold;
  }
`;

export const ButtonText = styled.p<TypographyProps>`
  font-family: ${Font.BOLD};
  color: ${Color.WHITE};
  font-size: 0.875rem;
  line-height: 1rem;
  letter-spacing: 0.04em;
  margin-bottom: 0;

  @media ${device.laptopL} {
    font-size: 1.125rem;
    line-height: 1.3125rem;
  }
`;

export const Superscript = styled.sup<TypographyProps>`
  font-size: 50%;
  top: -1em;
  font-family: ${Font.LIGHT};
  padding-left: 0.2em;
`;
