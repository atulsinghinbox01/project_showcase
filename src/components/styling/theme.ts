export enum Color {
  PRIMARY = '#24956A',
  DARKGREY = '#4C4C4C',
  LIGHTGREY = '#343434',
  DARK = '#333333',
  SEMIDARK = '#696969',
  WHITE = '#FFFFFF',
}

export enum Font {
  BOLD = 'EuclidCircular-B-Bold',
  SEMIBOLD = 'EuclidCircular-B-Semibold',
  REGULAR = 'EuclidCircular-B-Regular',
  MEDIUM = 'EuclidCircular-B-Medium',
  LIGHT = 'EuclidCircular-B-Light',
}
