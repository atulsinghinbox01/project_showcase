import * as React from 'react';
import { Container } from 'reactstrap';
import { WhiteNavbar } from '../components/WhiteNavbar';
import { AboutHeroSection } from '../sections/AboutHeroSection';
import { AdvisorsSection } from '../sections/AdvisorsSection';
import { FooterSection } from '../sections/FooterSection';
import { ImpactTextSection } from '../sections/ImpactTextSection';
import { InvestorLogoSection } from '../sections/InvestorLogoSection';
import { OurStorySection } from '../sections/OurStorySection';
import { TeamSection } from '../sections/TeamSection';
import { ValuesSection } from '../sections/ValuesSection';

const About: React.FunctionComponent = () => {
  return (
    <Container fluid>
      <WhiteNavbar isWhiteLinkText={false} />
      <AboutHeroSection />
      <OurStorySection />
      <ValuesSection />
      <TeamSection />
      <InvestorLogoSection />
      <AdvisorsSection />
      <ImpactTextSection />
      <FooterSection />
    </Container>
  );
};

export default About;
