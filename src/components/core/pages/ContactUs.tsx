import '../css/Terms.css';
import '../css/ContactUs.css';
import '../../css/sections.css';
import '../../css/bootstrap-overrides.css';
import '../../css/colors.css';
import * as React from 'react';
import { Helmet } from 'react-helmet';
import { Col, Container, Row } from 'reactstrap';
import { H1, H3, P } from '../../styling/Typography';
import { OpenAdaChatbotButton } from '../components/OpenAdaChatbotButton';
import { WhiteNavbar } from '../components/WhiteNavbar';
import { FooterSection } from '../sections/FooterSection';

const ContactUs: React.FunctionComponent = () => {
  return (
    <Container fluid className="landing-container">
      <Helmet>
        <title>Contact Us</title>
      </Helmet>
      <WhiteNavbar isWhiteLinkText={false} />
      <Row id="override" className="justify-content-center section-y-padding">
        <Col sm="12" md="10" lg="8">
          <H1 className="pb-big">Contact Us</H1>
          <div className="pb-big">
            <H3 className="pb-big light-green">Need Support?</H3>
            <P large>Our Custom Care Team is here for you.</P>
            <P large>
              We are committed to making the best use of your time and taking your questions and
              concerns as soon as we can.
            </P>
            <P large>
              How to reach us:
              <ul>
                <li>
                  For the fastest response, chat with our new bot, Jess, 24/7{' '}
                  <OpenAdaChatbotButton>here</OpenAdaChatbotButton>.
                </li>
                <li>
                  If she can't find a solution, you'll be routed to chat with a Customer Care
                  Associate or submit a ticket—we promise you'll receive a response within 24 hours.
                </li>
              </ul>
            </P>
          </div>
        </Col>
      </Row>
      <FooterSection />
    </Container>
  );
};

export default ContactUs;
