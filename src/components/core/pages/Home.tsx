import * as React from 'react';
import { Container } from 'reactstrap';
import { CTA_MOBILE_URL, isOnMobileDevice } from '../../../util';
import { WhiteNavbar } from '../components/WhiteNavbar';
import { FixedScrollSection } from '../sections/FixedScrollSection';
import { FooterSection } from '../sections/FooterSection';
import { HeroSection } from '../sections/HeroSection';
import { ImpactSection } from '../sections/ImpactSection';
import { MediaLogoSection } from '../sections/MediaLogoSection';
import { MembersSections } from '../sections/MembersSection';
import { PFMSection } from '../sections/PFMSection';
import { PricingSection } from '../sections/PricingSection';
import { RatingSection } from '../sections/RatingSection';
import { SecuritySection } from '../sections/SecuritySection';

const Home: React.FunctionComponent = () => {
  const textMeTheAppUrl = '/text-me-the-app';
  const isMobile = isOnMobileDevice();
  return (
    <Container fluid>
      <WhiteNavbar isWhiteLinkText={false} />
      <HeroSection
        ctaDesktopUrl={textMeTheAppUrl}
        ctaMobileUrl={CTA_MOBILE_URL}
        disclaimerNumber={1}
      />
      <MediaLogoSection />
      <FixedScrollSection
        approvalDisclaimerNumber={2}
        cbCreditScoreDisclaimerNumber={3}
        overdraftDisclaimerNumber={4}
      />
      <PFMSection />
      <PricingSection />
      <MembersSections />
      <ImpactSection disclaimerNumber={5} />
      <RatingSection
        mobileDownload={isMobile}
        ctaDesktopUrl={textMeTheAppUrl}
        ctaMobileUrl={CTA_MOBILE_URL}
      />
      <SecuritySection rowClassName="pt-5 grey-bg" />
      <FooterSection
        approvalDisclaimerNumber={2}
        cbCreditScoreDisclaimerNumber={3}
        overdraftDisclaimerNumber={4}
        impactDisclaimerNumber={5}
      />
    </Container>
  );
};

export default Home;
