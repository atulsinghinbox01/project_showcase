import * as React from 'react';
import { Col } from 'reactstrap';

interface Props {
  name: string;
  position: string;
  img: string;
  linkedInUrl: string;
  className?: string;
}

const RosterItem: React.FunctionComponent<Props> = (props) => {
  const { img, position, name, linkedInUrl } = props;
  return (
    <Col
      xs="12"
      sm="6"
      md="6"
      lg="4"
      xl="4"
      className="justify-content-center text-center euclid-bold pb-5"
    >
      <img className="roster-face" src={img} />
      <div className="roster-title dark-green">{position}</div>
      <div className="roster-person d-flex align-items-center justify-content-center">
        {name}
        <a
          href={linkedInUrl}
          target="_blank"
          rel="noopener noreferrer"
          className="d-flex justify-content-centre pl-1"
        >
          <img src="/v3/linkedin.png" className="linked-in" />
        </a>
      </div>
    </Col>
  );
};

export default RosterItem;
