import '../css/CelebrityModal.css';
import * as React from 'react';
import Modal from 'react-modal';
import { Button, Col, Container, Row } from 'reactstrap';
import { CelebrityBacker } from '../../../models/celebrity';
import { celebrityBackers } from '../../../utils/celebrity';
import { ButtonText, H2, P } from '../../styling/Typography';

interface CelebrityModalProps {
  isOpen: boolean;
  closeModal: () => void;
  name: string;
  imgUrl: string;
  description: string;
}

const CelebrityModal = (props: CelebrityModalProps) => {
  const { isOpen, closeModal, name, imgUrl, description } = props;
  return (
    <Modal
      contentLabel={`${name} Modal`}
      isOpen={isOpen}
      className="modal-content"
      overlayClassName="modal-overlay"
      shouldCloseOnOverlayClick
      onRequestClose={closeModal}
    >
      <Container>
        <Row>
          <Col>
            <div className="d-flex flex-column text-center">
              <H2 className="mb-4">{name}</H2>
              <div className="mb-4 d-flex justify-content-center" style={{ height: 128 }}>
                <img height="100%" src={imgUrl} />
              </div>
              <P className="pb-3">{description}</P>
              <Button className="get-button py-4" onClick={closeModal}>
                <ButtonText>Close</ButtonText>
              </Button>
            </div>
          </Col>
        </Row>
      </Container>
    </Modal>
  );
};

interface Props {
  currentOpenModal: CelebrityBacker | undefined;
  closeModal: () => void;
}

export const CelebrityModalGroup = (props: Props) => {
  const { currentOpenModal, closeModal } = props;

  return (
    <>
      {celebrityBackers.map((celebrity) => (
        <CelebrityModal
          key={celebrity.name}
          isOpen={currentOpenModal === celebrity.name}
          closeModal={closeModal}
          name={celebrity.name}
          imgUrl={celebrity.bannerUrl}
          description={celebrity.description}
        />
      ))}
    </>
  );
};
