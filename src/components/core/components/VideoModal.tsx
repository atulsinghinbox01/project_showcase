import * as React from 'react';
import Modal from 'react-modal';
import { Col, Container, Row } from 'reactstrap';

interface Props {
  isOpen: boolean;
  closeModal: any;
  youtubeUrl: string;
  name: string;
}

const customStyles = {
  content: {
    top: '0px',
    left: '0px',
    right: '0px',
    bottom: '0px',
  },
};

export const VideoModal = (props: Props) => {
  const { isOpen, closeModal, youtubeUrl } = props;
  return (
    <Modal
      contentLabel="Activate Safety Net"
      isOpen={isOpen}
      style={customStyles}
      shouldCloseOnOverlayClick
      onRequestClose={closeModal}
    >
      <Container>
        <Row>
          <Col>
            <div onClick={closeModal} className="d-flex justify-content-end pointer">
              <img src="/close_x_icon.svg" />
            </div>
            <div className="d-flex flex-column text-center">
              <h2 style={{ fontSize: '48px' }}>Your Safety Net</h2>
              {youtubeUrl}
            </div>
          </Col>
        </Row>
      </Container>
    </Modal>
  );
};
