import React, { SFC } from 'react';
import ReactSVG from 'react-svg';
import { Button } from 'reactstrap';
import { ButtonType, trackConversion, trackEvent } from '../../../utils/tracking';
import { ButtonText } from '../../styling/Typography';
import { scrollTop } from '../sections/FooterSection';

interface Props {
  buttonStr: string;
  link: string;
  id: string;
  trackCategory?: string;
  className?: string;
  webOnly?: boolean;
}

const trackClick = (category: string) => {
  trackEvent(category, 'click', 'download_button');
  trackConversion(ButtonType.DOWNLOAD_BUTTON);
};

const determineContents = (buttonStr, webOnly) => {
  const userAgent = navigator.userAgent || navigator.vendor;
  if (
    !webOnly &&
    (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i))
  ) {
    return (
      <div className="d-flex align-items-center">
        <ReactSVG className="pr-2 py-1" path="/rebrand/ios-btn-logo.svg" />
        {buttonStr}
      </div>
    );
  } else if (!webOnly && userAgent.match(/Android/i)) {
    return (
      <div className="d-flex align-items-center">
        <ReactSVG className="pr-2 py-1" path="/rebrand/android-btn-logo.svg" />
        {buttonStr}
      </div>
    );
  } else {
    return <div>{buttonStr}</div>;
  }
};

const isDesktop = () => {
  const userAgent = navigator.userAgent || navigator.vendor;
  if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
    return false;
  } else if (userAgent.match(/Android/i)) {
    return false;
  }
  return true;
};

export const DarkGreenButton: SFC<Props> = (props) => {
  const { buttonStr, link, id, webOnly, className } = props;
  const padding = isDesktop() ? `py-3 px-4` : `py-2 px-5`;
  const ctaStr = isDesktop() || webOnly ? buttonStr : 'Download';
  const category = props.trackCategory || 'sign_up_mobile_web';
  return (
    <div>
      <Button
        id={id}
        onClick={() => trackClick(category)}
        href={link}
        className={`get-button ${padding} ${className}`}
      >
        <ButtonText className="px-lg-5">{determineContents(ctaStr, webOnly)}</ButtonText>
      </Button>
    </div>
  );
};

interface ScrollToTopProps {
  ctaText: string;
}
export const DGScrollToTopDesktopButton: SFC<ScrollToTopProps> = (props) => {
  return (
    <Button onClick={() => scrollTop()} className="get-button py-3 px-5">
      <div className="text-white get-button px-lg-5">{props.ctaText}</div>
    </Button>
  );
};

export const DarkButton: SFC<Props> = (props) => {
  const { buttonStr, link, id, className, webOnly } = props;
  const ctaStr = isDesktop() || webOnly ? buttonStr : 'Download';
  const category = props.trackCategory || 'sign_up_mobile_web';
  return (
    <Button
      id={id}
      onClick={() => trackClick(category)}
      href={link}
      className="get-button-footer py-3 px-4"
    >
      <ButtonText className={`px-3 px-lg-5 ${className}`}>
        {determineContents(ctaStr, webOnly)}
      </ButtonText>
    </Button>
  );
};

export const NavGreenButton: SFC<Props> = (props) => {
  const { buttonStr, link, id } = props;
  const padding = isDesktop() ? `py-2 px-5` : `py-2 px-3`;
  const category = props.trackCategory || 'sign_up_mobile_web';

  return (
    <Button
      id={id}
      onClick={() => trackClick(category)}
      href={link}
      className={`get-button ${padding}`}
    >
      <ButtonText className="text-white get-button">{buttonStr}</ButtonText>
    </Button>
  );
};
