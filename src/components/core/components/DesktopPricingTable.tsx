import '../../css/bootstrap-overrides.css';
import '../../css/sections.css';
import * as React from 'react';
import { Table } from 'reactstrap';
import { H3, H4, P, Superscript } from '../../styling/Typography';

const features = [
  {
    title: 'Credit Builder',
    description: 'Build your credit and save.',
    basic: false,
  },
  {
    title: 'Credit Protect & Secure',
    description: 'Tools and full credit reports to help you monitor and improve your credit.',
    basic: false,
  },
  {
    title: 'Quick cash',
    description: 'Need cash ASAP? No problem.*',
    basic: false,
  },
  {
    title: 'Auto Advances',
    description: "If you're at risk of an overdraft we'll automatically make sure you're covered.*",
    basic: false,
  },
  {
    title: 'Flexible Repayment',
    description: 'Need more time? No problem. Payback early? We have you covered.',
    basic: false,
  },
  {
    title: 'Identity Theft Protection',
    description: 'Protect and insure your identity with $1M identity theft insurance.',
    basic: false,
  },
  {
    title: 'Earn Extra',
    description: 'Find opportunities to increase your overall income.',
    basic: true,
  },
  {
    title: 'Finance Helper',
    description: 'Get practical information on your past, current and future financial outcomes.',
    basic: true,
  },
  {
    title: 'Insights',
    description: 'Personalized content and tips to guide your financial health.',
    basic: true,
  },
];

const DesktopPricingTable: React.FunctionComponent<{ impactDisclaimerNumber: number }> = ({
  impactDisclaimerNumber,
}) => {
  return (
    <Table id="override" bordered className="section-y-padding">
      <thead>
        <tr className="section-top-padding">
          <th className="align-middle">
            <H3 className="px-3">Two simple plans. No hidden fees, “tips,” or fine print.</H3>
          </th>
          <th className="text-center p-3">
            <H3 className="pt-2">Plus</H3>
            <P className="font-regular">
              Get quick cash advances, find side gigs and save an average of $500 a year in fees
              <Superscript>{impactDisclaimerNumber}</Superscript>
            </P>
            <H4 className="primary">$9.99/month</H4>
          </th>
          <th className="text-center p-3">
            <H3 className="pt-2">Free</H3>
            <P className="font-regular">Get alerts, financial insights, and more</P>
            <H4 className="primary">$0</H4>
          </th>
        </tr>
      </thead>
      <tbody>
        {features.map((h) => {
          const basicPlanIcon = h.basic ? '/v3/membership-checkmark.svg' : '/v4/icons/x.svg';
          return (
            <tr key={h.title}>
              <td className="align-middle">
                <div className="pl-3">
                  <P className="font-bold dark mb-1">{h.title}</P>
                  <P className="font-regular">{h.description}</P>
                </div>
              </td>
              <td className="text-center align-middle">
                <img src="/v3/membership-checkmark.svg" />
              </td>
              <td className="text-center align-middle">
                <img src={basicPlanIcon} />
              </td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default DesktopPricingTable;
