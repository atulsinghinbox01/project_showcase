import '../css/OpenAdaChatbotButton.css';
import * as React from 'react';

export const OpenAdaChatbotButton: React.FC = ({ children }) => {
  const openAdaChatbot = () => {
    window.adaEmbed.toggle();
  };

  return (
    <span onClick={openAdaChatbot} className="primary font-bold" id="open-chatbot-button">
      {children}
    </span>
  );
};
