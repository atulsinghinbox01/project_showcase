import * as React from 'react';
import { Card, CardBody } from 'reactstrap';

interface Props {
  header: string;
  copy: any;
  className?: string;
  paddingOverride?: string;
}

const AboutCard: React.FunctionComponent<Props> = (props) => {
  return (
    <Card className="p-2 about-card">
      <CardBody className={`d-flex justify-content-center flex-column ${props.paddingOverride}`}>
        <div>
          <div className="light-green about-card-title text-center euclid-bold">{props.header}</div>
          <div className="body-font text-center px-4">{props.copy}</div>
        </div>
      </CardBody>
    </Card>
  );
};

export default AboutCard;
