import * as React from 'react';
import { Card, CardBody } from 'reactstrap';
import { P } from '../../../styling/Typography';

interface Props {
  header: string;
  text: string;
  iconUri?: string;
  className?: string;
}

const MembershipFactCard: React.FunctionComponent<Props> = (props) => {
  const { iconUri = '/v3/membership-checkmark.svg' } = props;
  return (
    <Card className={`mb-2 p-2 membership-card ${props.className}`}>
      <CardBody>
        <div className="d-flex justify-content-between">
          <div className="pr-3 pr-md-0">
            <P className="dark font-bold mb-1">{props.header}</P>
            <P className="font-regular mb-0">{props.text}</P>
          </div>
          <div className="d-flex align-middle">
            <img style={{ width: '30px' }} src={iconUri} />
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

export default MembershipFactCard;
