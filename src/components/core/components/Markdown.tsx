import '../css/Terms.css';
import { isNil } from 'lodash';
import * as React from 'react';
import ReactMarkdown from 'react-markdown';
import rehypeRaw from 'rehype-raw';
import gfm from 'remark-gfm';
import { Color } from '../../styling/theme';

interface Props {
  markdownText: string;
}

export const Markdown: React.FunctionComponent<Props> = ({ markdownText }) => {
  return (
    <ReactMarkdown
      rehypePlugins={[rehypeRaw]}
      remarkPlugins={[gfm]}
      components={{
        h1: ({ children }) => <h1 style={{ color: `${Color.PRIMARY}` }}>{children}</h1>,
        td: ({ children }) => (
          <td style={{ border: `1px solid ${Color.LIGHTGREY}`, padding: 5 }}>{children}</td>
        ),
        strong: ({ children }) => <b>{children}</b>,
        th: ({ children }) => (
          <th>{!isNil(children) && <b style={{ padding: 5 }}>{children}</b>}</th>
        ),
        table: ({ children }) => (
          <>
            <table
              style={{
                border: `1px solid ${Color.LIGHTGREY}`,
                borderCollapse: 'collapse',
                width: '100%',
              }}
            >
              {children}
            </table>
            <br />
          </>
        ),
      }}
    >
      {markdownText}
    </ReactMarkdown>
  );
};
