import '../css/ProductInfoCard.css';
import '../../css/bootstrap-overrides.css';
import React, { SFC } from 'react';
import { H3, P } from '../../styling/Typography';

interface InfoCardProps {
  imgUri: string;
  title: string;
  subtext: string;
}
export const ProductInfoCard: SFC<InfoCardProps> = (props) => {
  return (
    <div className="text-center product-info-card p-3">
      <div>
        <img style={{ width: '60%' }} src={props.imgUri} />
      </div>
      <div className="p-2">
        <H3 className="dark">{props.title}</H3>
        <P>{props.subtext}</P>
      </div>
    </div>
  );
};
