import React, { FunctionComponent } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { getWebAppUrl } from '../../../util';
import { ButtonType, trackConversion } from '../../../utils/tracking';

interface Props {
  isOpen: boolean;
  toggle: any;
  logoUrl: string;
  whiteMode: boolean;
  ctaLink: string;
  ctaText: string;
  hideLinks: boolean;
  logoNavUrl: string;
}

export const HamburgerNav: FunctionComponent<Props> = (props: Props) => {
  const linkColor = props.whiteMode ? 'white' : 'blk';
  const downloadLinkColor = props.whiteMode ? 'white' : 'dark-green';
  const { ctaLink, ctaText, hideLinks, logoNavUrl } = props;

  return (
    <Navbar color="faded" inverse={props.whiteMode} light={!props.whiteMode}>
      <NavbarBrand href={logoNavUrl} className="px-0">
        <img className="nav-logo" src={props.logoUrl} />
      </NavbarBrand>
      <NavbarToggler
        style={{ border: 'none', outline: 'none' }}
        className="pb-3 pt-3 px-0"
        onClick={props.toggle}
      />
      <Collapse className="text-center" isOpen={props.isOpen} navbar>
        <Nav navbar>
          {!hideLinks && (
            <>
              <NavItem>
                <NavLink className={`${linkColor} euclid-bold`} href="/about">
                  Our story
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className={`${linkColor} euclid-bold`} href="/impact">
                  Our impact
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className={`${linkColor} euclid-bold`} href="/faq">
                  FAQ
                </NavLink>
              </NavItem>
            </>
          )}
          <NavItem>
            <NavLink
              className={`${downloadLinkColor} euclid-bold`}
              href={ctaLink}
              onClick={() => trackConversion(ButtonType.DOWNLOAD_BUTTON)}
            >
              {ctaText}
            </NavLink>
          </NavItem>
          {!hideLinks && (
            <NavItem>
              <NavLink className={`${linkColor} euclid-bold`} href={getWebAppUrl('/login')}>
                Log In
              </NavLink>
            </NavItem>
          )}
        </Nav>
      </Collapse>
    </Navbar>
  );
};
