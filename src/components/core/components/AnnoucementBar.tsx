import '../../css/bootstrap-overrides.css';
import '../../css/colors.css';
import React from 'react';
import { ReactNode } from 'react-redux';
import { P } from '../../styling/Typography';

interface Props {
  text: ReactNode;
}
export const AnnoucementBar: React.FunctionComponent<Props> = (props) => {
  const { text } = props;

  return (
    <div id="override" className="d-flex flex-column text-center bg-color-dark ">
      <P large className="pt-2 white">
        {text}
      </P>
    </div>
  );
};
