import { OneLinkResponse } from '../actions/oneLinkActions';
import { GetPlaidConfig, UserDataResponse } from '../models/plaid';

export interface AppState {
  errors: ErrorState;
  api: ApiState;
  authState: AuthState;
  config?: GetPlaidConfig;
  userData?: UserDataResponse;
  utmData: UtmData;
  webAppCta: string;
  oneLinkUrl?: OneLinkResponse;
}

export interface UtmData {
  campaignSource?: string | string[];
  campaignMedium?: string | string[];
  campaignName?: string | string[];
  campaignTerm?: string | string[];
  campaignContent?: string | string[];
  gclid?: string | string[];
  irclickid?: string;
}

export interface AuthState {
  token?: string;
  registered: boolean;
}

export interface ApiState {
  [action: string]: ApiCallState;
}

interface ApiCallState {
  requesting: boolean; // in progress
  success: boolean;
  error: Error;
}

export interface ErrorState {
  lastHttpStatus: number;
  message?: string;
}
