import { TypedReducer } from 'redoodle';
import { ApiAction } from '../actions/apiActions';
import { AddItemApi, GetConfigApi, GetUserDataApi, NewUserApi } from '../actions/plaidActions';
import { ApiState } from '../store';

// @ts-ignore
const apiActions: ApiAction<any, any>[] = [GetConfigApi, NewUserApi, AddItemApi, GetUserDataApi];

// TODO this reducer is super hacky right now, fix this later
const builder = TypedReducer.builder<ApiState>();

apiActions.forEach((action) => {
  builder.withHandler(action.REQUEST.TYPE, (state) => {
    const apiCallState = state[action.TYPE] || {
      requesting: false,
      success: false,
      error: undefined,
    };
    return { ...state, [action.TYPE]: { ...apiCallState, requesting: true } };
  });

  builder.withHandler(action.SUCCESS.TYPE, (state) => {
    const apiCallState = state[action.TYPE];
    return { ...state, [action.TYPE]: { ...apiCallState, requesting: false, success: true } };
  });

  builder.withHandler(action.FAILURE.TYPE, (state, payload) => {
    const apiCallState = state[action.TYPE];
    return { ...state, [action.TYPE]: { ...apiCallState, requesting: false, error: payload } };
  });
});

export default builder.build();
