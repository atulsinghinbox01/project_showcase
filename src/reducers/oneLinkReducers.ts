import { TypedReducer } from 'redoodle';
import { CreateOneLinkApi, OneLinkResponse } from '../actions/oneLinkActions';

const builder = TypedReducer.builder<OneLinkResponse>();

builder.withHandler(CreateOneLinkApi.SUCCESS.TYPE, (state, payload) => {
  return { ...state, ...payload };
});

export default builder.build();
