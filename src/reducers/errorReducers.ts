import { TypedReducer } from 'redoodle';
import { RecordErrorAction } from '../actions/errorActions';
import { ErrorState } from '../store';

const builder = TypedReducer.builder<ErrorState>();

builder.withHandler(RecordErrorAction.TYPE, (state, payload) => {
  return { ...state, ...payload };
});

export default builder.build();
