import { TypedReducer } from 'redoodle';
import { AddItemApi, GetUserDataApi } from '../actions/plaidActions';
import { UserDataResponse } from '../models/plaid';

const builder = TypedReducer.builder<UserDataResponse>();

builder.withHandler(GetUserDataApi.SUCCESS.TYPE, (state, payload) => {
  return { ...state, ...payload };
});

builder.withHandler(AddItemApi.SUCCESS.TYPE, (state, payload) => {
  return { ...state, ...payload };
});

export default builder.build();
