import { combineReducers, composeReducers } from 'redoodle';
import { AppState } from '../store';
import api from './apiReducers';
import authState from './authStateReducers';
import config from './configReducers';
import errors from './errorReducers';
import oneLinkUrl from './oneLinkReducers';
import userData from './plaidReducers';
import utmData from './utmReducers';
import webAppCta from './webAppCtaReducers';

export const reducers = {
  api,
  errors,
  authState,
  config,
  userData,
  utmData,
  oneLinkUrl,
  webAppCta,
};
const coreReducer = combineReducers<AppState>(reducers);
export const rootReducer = composeReducers(coreReducer);
