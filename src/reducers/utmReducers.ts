import { TypedReducer } from 'redoodle';
import { SetUtmAction } from '../actions/utmActions';
import { UtmData } from '../store/AppState';

const builder = TypedReducer.builder<UtmData>();

builder.withHandler(SetUtmAction.TYPE, (state, payload) => {
  return { ...state, ...payload };
});

export default builder.build();
