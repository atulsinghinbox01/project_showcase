import { TypedReducer } from 'redoodle';
import { SetWebAppCtaAction } from '../actions/webAppCtaActions';

const builder = TypedReducer.builder<string>();

builder.withHandler(SetWebAppCtaAction.TYPE, (state, payload) => {
  return payload;
});

export default builder.build();
