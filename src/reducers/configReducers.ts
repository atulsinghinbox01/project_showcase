import { TypedReducer } from 'redoodle';
import { GetConfigApi } from '../actions/plaidActions';
import { GetPlaidConfig } from '../models/plaid';

const builder = TypedReducer.builder<GetPlaidConfig>();

builder.withHandler(GetConfigApi.SUCCESS.TYPE, (state, payload) => {
  return { ...state, ...payload };
});

export default builder.build();
