import { TypedAction } from 'redoodle';

export const SetWebAppCtaAction = TypedAction.define('SET_WEB_APP_CTA')<string>();
