import { TypedAction } from 'redoodle';
import { REHYDRATE } from 'redux-persist';

export const RehydrateAction = TypedAction.defineWithoutPayload(REHYDRATE)();
