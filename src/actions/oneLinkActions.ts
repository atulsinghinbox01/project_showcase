import { ApiActions } from './apiActions';

/**
 * Attribution link params that allow AppsFlyer to record engagement with an ad
 * For more context re: what each of these params represent, see support.appsflyer.com/hc/en-us/articles/207447163-Attribution-link-structure-and-parameters#attribution-link-parameters
 */
export interface OneLinkParams {
  c: string | string[];
  af_adset: string | string[];
  af_channel: string | string[];
  af_ad_id?: string | string[];
  af_adset_id?: string | string[];
  referral_source_id?: string | string[];
  clickid?: string;
  pid?: string;
}

export interface OneLinkRequest {
  data: OneLinkParams;
}

export interface SendSmsWithOneLinkAttributionReq extends OneLinkParams {
  phoneNumber: string;
}

export interface OneLinkResponse {
  oneLinkUrl: string;
}

export const CreateOneLinkApi = ApiActions.create<OneLinkRequest, OneLinkResponse>(
  'CREATE_ONE_LINK',
  {
    method: 'POST',
    url: '/api/site/onelink/new',
  },
);

export const CreateOneLinkAndSendSMS = ApiActions.create<SendSmsWithOneLinkAttributionReq, void>(
  'SEND_SMS_WITH_ONE_LINK_ATTRIBUTION',
  {
    method: 'POST',
    url: '/api/download/v3/link',
  },
);
