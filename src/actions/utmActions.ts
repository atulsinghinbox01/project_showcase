import { TypedAction } from 'redoodle';
import { UtmData } from '../store';

export const SetUtmAction = TypedAction.define('SET_UTM')<UtmData>();
