import {
  AddItemRequest,
  AuthResponse,
  GetPlaidConfig,
  NewUserRequest,
  UserDataResponse,
} from '../models/plaid';
import { ApiActions } from './apiActions';

export const GetConfigApi = ApiActions.createWithEmptyRequest<GetPlaidConfig>('GET_PLAID_CONFIG', {
  method: 'GET',
  url: '/api/v1/plaid/config',
});

export const NewUserApi = ApiActions.create<NewUserRequest, AuthResponse>('NEW_USER', {
  method: 'POST',
  url: '/api/v1/fees/user/new',
});

export const AddItemApi = ApiActions.create<AddItemRequest, UserDataResponse>('ADD_ITEM', {
  method: 'POST',
  url: '/api/v1/plaid/addItem',
});

export const GetUserDataApi = ApiActions.createWithEmptyRequest<UserDataResponse>('GET_USER_DATA', {
  method: 'GET',
  url: '/api/v1/plaid/userData',
});
