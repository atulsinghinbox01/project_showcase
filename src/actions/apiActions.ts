/* eslint-disable */
import { bindActionCreators, Action } from 'redux';
import { TypedAction } from 'redoodle';
import { ThunkAction } from 'redux-thunk';

import { AppState } from '../store';
import { RecordErrorAction } from './errorActions';

export interface ApiLogoutResp {}

export interface ApiAction<RR, SR> {
  TYPE: string;
  REQUEST: TypedAction.Definition<string, RR>;
  SUCCESS: TypedAction.Definition<string, SR>;
  FAILURE: TypedAction.Definition<string, Error>;
  call: (body: RR, pathParams?: any) => ThunkAction<{}, AppState, {}, Action>;
}

// @ts-ignore
interface ApiActionEmptyRequest<SR> extends ApiAction<null, SR> {
  REQUEST: TypedAction.NoPayloadDefinition<string>;
  call: (pathParams?: any) => ThunkAction<{}, AppState, {}, Action>;
}

export const ApiLogoutUserAction = TypedAction.define('API_LOGOUT_USER')<ApiLogoutResp>();

export namespace ApiActions {
  export function create<RR, SR>(prefix: string, requestConfig): ApiAction<RR, SR> {
    const REQUEST = TypedAction.define(`${prefix}_REQUEST`)<RR>();
    const SUCCESS = TypedAction.define(`${prefix}_SUCCESS`)<SR>();
    const FAILURE = TypedAction.define(`${prefix}_FAILURE`)<Error>();
    const creator = createApiActionCreator(REQUEST, SUCCESS, FAILURE, requestConfig);
    return {
      TYPE: prefix,
      REQUEST,
      SUCCESS,
      FAILURE,
      call: creator,
    };
  }

  export function createWithEmptyRequest<SR>(
    prefix: string,
    requestConfig,
  ): ApiActionEmptyRequest<SR> {
    const REQUEST = TypedAction.defineWithoutPayload(`${prefix}_REQUEST`)();
    const SUCCESS = TypedAction.define(`${prefix}_SUCCESS`)<SR>();
    const FAILURE = TypedAction.define(`${prefix}_FAILURE`)<Error>();
    const creator = createApiActionCreator(REQUEST as any, SUCCESS, FAILURE, requestConfig);
    return {
      TYPE: prefix,
      REQUEST,
      SUCCESS,
      FAILURE,
      call: (pathParams?: any) => creator(undefined, pathParams),
    };
  }
}

function createApiActionCreator<
  R extends string,
  S extends string,
  F extends string,
  RR = {},
  SR = {}
>(
  requestAction: TypedAction.Definition<R, RR>,
  successAction: TypedAction.Definition<S, SR>,
  failureAction: TypedAction.Definition<F, Error>,
  requestConfig: {
    url?: string;
    method: string;
    headers?: {};
  },
): (body: RR, pathParams: any) => ThunkAction<{}, AppState, {}, Action> {
  let { method, url, headers } = requestConfig;
  return (body: RR, pathParams: any) => {
    return (dispatch, getState) => {
      const {
        dispatchRequest,
        dispatchSuccess,
        dispatchFailure,
        dispatchRecordError,
        dispatchLogout,
      } = bindActionCreators(
        {
          dispatchRequest: requestAction.create,
          dispatchSuccess: successAction.create,
          dispatchFailure: failureAction.create,
          dispatchRecordError: RecordErrorAction.create,
          dispatchLogout: ApiLogoutUserAction.create,
        },
        dispatch,
      );
      const {
        authState: { token },
      } = getState();
      if (token) {
        headers = { ...headers, Authorization: `Bearer ${token}` };
      }
      headers = { ...headers, 'Content-Type': 'application/json' };
      dispatchRequest(body);
      return fetch(url, { method, headers, body: JSON.stringify(body) })
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else {
            return Promise.reject(res);
          }
        })
        .then(dispatchSuccess)
        .catch((res) => {
          if (res.status === 401) {
            dispatchLogout({});
          } else {
            res.json().then((json) => {
              const message = json.message || res.statusText || json;
              dispatchRecordError({ lastHttpStatus: res.status, message });
              // also hack for now to get around typing
              const err: any = new Error(message);
              err.response = res;
              dispatchFailure(err);
            });
          }
        });
    };
  };
}
