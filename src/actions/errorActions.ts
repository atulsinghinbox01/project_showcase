import { TypedAction } from 'redoodle';

export enum ErrorActionTypes {
  RECORD_ERROR = 'RECORD_ERROR',
}

interface RecordErrorPayload {
  lastHttpStatus: number;
  message: string;
}

export const RecordErrorAction = TypedAction.define(ErrorActionTypes.RECORD_ERROR)<
  RecordErrorPayload
>();
