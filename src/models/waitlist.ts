export interface WaitlistUserRequest {
  email: string;
  copyId: string;
}

export interface WaitlistUserResponse {
  token: string;
}
