export enum CelebrityBacker {
  ASHTON_KUTCHER = 'Ashton Kutcher',
  WILL_SMITH = 'Will Smith',
  KEVIN_DURANT = 'Kevin Durant',
}

export interface CelebrityBackerDetails {
  name: CelebrityBacker;
  description: string;
  headshotUrl: string;
  bannerUrl: string;
}
