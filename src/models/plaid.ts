export interface GetPlaidConfig {
  plaidEnv: string;
  plaidPublicKey: string;
  webhookUrl: string;
}

export interface NewUserRequest {
  email: string;
}

export interface AuthResponse {
  token: string;
}

export interface AddItemRequest {
  publicToken: string;
}

export interface FeesByCategory {
  'Uncategorized Fees'?: number;
  'Overdraft Fees'?: number;
  'ATM Fees'?: number;
  'Late Payment Fees'?: number;
  'Fraud Dispute Fees'?: number;
  'Foreign Transaction Fees'?: number;
  'Wire Transfer Fees'?: number;
  'Cash Advance Fees'?: number;
  'Excess Activity Fees'?: number;
}

export interface UserDataResponse {
  linked: boolean;
  accounts: number;
  transactionCount: number;
  feesByCategory?: FeesByCategory;
  feeTransactions: { [fee in keyof FeesByCategory]: PlaidTransaction[] };
  firstDate?: string;
  balanceRevenue?: number;
  kickoffId: string;
  institutionName: string;
}

export interface PlaidTransaction {
  userId: number;
  accountId: string;
  transactionId: string;
  categoryId?: string;
  transactionType?: string;
  amount: number;
  date: string; // TODO make this Date object?
  address?: string;
  city?: string;
  state?: string;
  zip?: string;
  lat?: number;
  lon?: number;
  pending: boolean;
  name: string;
}
