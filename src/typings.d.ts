// Types found at https://dev.appsflyer.com/hc/docs/onelinkurlgenerator
interface OneLinkUrlGeneratorParams {
  oneLinkURL: string;
  pidKeysList?: string[];
  pidOverrideList?: object;
  pidStaticValue?: string;
  campaignKeysList?: string[];
  campaignStaticValue?: string;
  gclIdParam?: string;
  skipList?: string[];
}

interface AdaEmbed {
  /**
   * Programatically open / close Chat window
   * https://adasupport.github.io/documentation/#toggle
   */
  toggle?: () => Promise<void>;
}

interface AdaSettings {
  /**
   * Where to mount the <iframe> (only needed if the default side drawer is not desired)
   * https://adasupport.github.io/documentation/#parentelement
   */
  parentElement?: string | HTMLElement;
  /**
   * Trigger side-effects when the Web Chat drawer is opened or closed
   * https://adasupport.github.io/documentation/#togglecallback
   */
  toggleCallback?: (isDrawerOpen: boolean) => void;
}

// Augmenting the window interface so we don't get a type error when accessing these properties
interface Window {
  tatari: {
    track: (name: string, detail?: object) => void;
  };
  AF: {
    OneLinkUrlGenerator: (params?: OneLinkUrlGeneratorParams) => void;
  };
  gtag: (command: string, ...commandParameters: string | object | Function) => void; // API reference for Global site tag found at https://developers.google.com/tag-platform/gtagjs/reference
  adaEmbed: AdaEmbed;
  adaSettings: AdaSettings;
}
