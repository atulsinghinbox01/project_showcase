import { Middleware } from 'redux';
import { RehydrateAction } from '../actions/configActions';
import { SetUtmAction } from '../actions/utmActions';
import { SetWebAppCtaAction } from '../actions/webAppCtaActions';
import { getAttributionParamsFromQueryString, getWebAppUrl } from '../util';

export const attributionMiddleware: Middleware = (store) => (next) => (action) => {
  const result = next(action);
  /**
   * In the past, we used to grab URL / UTM params on store initialization, only
   * for it to be overwritten away by the Rehydrate action via redux-persist in
   * the case where a user visited with UTM params in the past
   * This allows us to grab the URL / UTM params AFTER the rehydrate so it ensures
   * that whatever is in the URL is what we want in our store
   * If there are no parameters in the URL but there are values from the reyhdrate,
   * the values from rehydrate are what we want in the store
   */
  if (RehydrateAction.is(action)) {
    const {
      location: { search },
    } = window;
    const currAttribution = getAttributionParamsFromQueryString(search);
    store.dispatch(SetUtmAction.create(currAttribution));

    // When a user navigates to the web app, we want to include their attribution params
    // in the link so that we can link them back to the ad they came from.
    const webAppSignUpCta = getWebAppUrl('/signup/email');
    store.dispatch(SetWebAppCtaAction.create(webAppSignUpCta));
  }

  return result;
};
